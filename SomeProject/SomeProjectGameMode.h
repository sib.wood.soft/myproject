// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SomeProjectGameMode.generated.h"

UCLASS(minimalapi)
class ASomeProjectGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASomeProjectGameMode();
};



