// Copyright Epic Games, Inc. All Rights Reserved.

#include "SomeProjectGameMode.h"
#include "SomeProjectCharacter.h"
#include "UObject/ConstructorHelpers.h"

ASomeProjectGameMode::ASomeProjectGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
