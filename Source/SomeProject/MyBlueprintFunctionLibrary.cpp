// Fill out your copyright notice in the Description page of Project Settings.


#include "MyBlueprintFunctionLibrary.h"

FVector UMyBlueprintFunctionLibrary::SetRTSBoxBounds(UBoxComponent* BoxComponent, FVector Bounds)
{
	float gridSize = 50.f;
	Bounds.X = floor(Bounds.X);
	Bounds.Y = floor(Bounds.Y);
	Bounds.Z = floor(Bounds.Z);
	Bounds *= gridSize;
	BoxComponent->SetBoxExtent(Bounds, true);
	BoxComponent->SetRelativeLocation(Bounds);
	return Bounds;
}
