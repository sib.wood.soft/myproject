// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class SomeProject : ModuleRules
{
	public SomeProject(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
